import { TypeOrmModuleOptions } from '@nestjs/typeorm'
import { ConfigService } from '@nestjs/config'

export const getTypeOrmConfig = async (
	configService: ConfigService
): Promise<TypeOrmModuleOptions> => ({
	type: 'postgres',
	host: 'localhost',
	port: configService.get('PG_PORT'),
	database: configService.get('PG_DB'),
	username: configService.get('PG_USERNAME'),
	password: configService.get('PG_PASSWORD'),
	autoLoadEntities: true,
	synchronize: true
})
