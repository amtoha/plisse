export type TypeRoles =
	| 'admin'
	| 'manager'
	| 'diller'
	| 'subdiller'
	| 'production'
	| undefined
