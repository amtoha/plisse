import { UsersEntity } from '../../users/users.entity'
import {
	CanActivate,
	ExecutionContext,
	ForbiddenException
} from '@nestjs/common'
import { Reflector } from '@nestjs/core'

export class AdminOnlyGuard implements CanActivate {
	constructor(private reflector: Reflector) {}
	canActivate(context: ExecutionContext): boolean {
		const request = context.switchToHttp().getRequest<{ user: UsersEntity }>()
		const user = request.user
		if (user.role !== 'admin')
			throw new ForbiddenException('Нет прав для выполнения запроса!')
		return true
	}
}
