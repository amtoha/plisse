import {
	BadRequestException,
	Injectable,
	NotFoundException,
	UnauthorizedException
} from '@nestjs/common'
import { AuthDto } from './dto/auth.dto'
import { InjectRepository } from '@nestjs/typeorm'
import { UsersEntity } from '../users/users.entity'
import { Repository } from 'typeorm'
import { JwtService } from '@nestjs/jwt'
import { compare, genSalt, hash } from 'bcryptjs'
import { RefreshTokenDto } from './dto/refreshToken.dto'

@Injectable()
export class AuthService {
	constructor(
		@InjectRepository(UsersEntity)
		private readonly usersRepository: Repository<UsersEntity>,
		private readonly jwtService: JwtService
	) {}

	returnUserFields(user: UsersEntity) {
		return {
			id: user.id,
			email: user.email,
			role: user.role,
			isActive: user.isActive
		}
	}

	async issueTokenPair(userId: number) {
		const data = { id: userId }

		const refreshToken = await this.jwtService.signAsync(data, {
			expiresIn: '14d'
		})

		const accessToken = await this.jwtService.signAsync(data, {
			expiresIn: '1h'
		})

		return { refreshToken, accessToken }
	}

	async validateUser(dto: AuthDto) {
		const user = await this.usersRepository.findOne({
			where: {
				email: dto.email
			},
			select: ['id', 'email', 'password', 'role', 'isActive']
		})
		if (!user) throw new NotFoundException('Пользователь не найден')

		const isValidPassword = await compare(dto.password, user.password)
		if (!isValidPassword) throw new UnauthorizedException('Неверный пароль')

		return user
	}

	async login(dto: AuthDto) {
		const user = await this.validateUser(dto)
		const tokens = await this.issueTokenPair(user.id)

		return { user: this.returnUserFields(user), ...tokens }
	}

	async register(dto: AuthDto) {
		const oldUser = await this.usersRepository.findOneBy({ email: dto.email })
		if (oldUser)
			throw new BadRequestException(
				'Пользователь с таким email уже есть в системе'
			)

		const salt = await genSalt(10)
		const newUser = await this.usersRepository.create({
			email: dto.email,
			password: await hash(dto.password, salt),
			role: dto.role,
			isActive: true
		})

		const user = await this.usersRepository.save(newUser)
		const tokens = await this.issueTokenPair(user.id)

		return { user: this.returnUserFields(user), ...tokens }
	}

	async getNewTokens({ refreshToken }: RefreshTokenDto) {
		if (!refreshToken)
			throw new UnauthorizedException('Необходима авторизация!')

		const result = await this.jwtService.verifyAsync(refreshToken)
		if (!result) throw new UnauthorizedException('Невалидный токен')

		const user = await this.usersRepository.findOneBy({ id: result.id })
		const tokens = await this.issueTokenPair(user.id)

		return { user: this.returnUserFields(user), ...tokens }
	}
}
