import { applyDecorators, UseGuards } from '@nestjs/common'
import { TypeRoles } from '../auth.interface'
import { JwtAuthGuard } from '../guards/jwt.guard'
import { AdminOnlyGuard } from '../guards/admin.guard'

export const Auth = (role?: TypeRoles) =>
	applyDecorators(
		role === 'admin'
			? UseGuards(JwtAuthGuard, AdminOnlyGuard)
			: UseGuards(JwtAuthGuard)
	)
