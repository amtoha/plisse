import { IsEmail, IsString, MinLength } from 'class-validator'
import { TypeRoles } from '../auth.interface'

export class AuthDto {
	@IsEmail()
	email: string

	@MinLength(6, { message: 'Минимальная длина пароля 6 символов' })
	@IsString()
	password: string

	@IsString()
	role?: TypeRoles
}
