import { TypeRoles } from '../../auth/auth.interface'

export class UpdateUserDto {
	email?: string
	password?: string
	role?: TypeRoles
	isActive?: boolean
}
