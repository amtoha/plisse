import { Body, Controller, Get, HttpCode, Put } from '@nestjs/common'
import { UsersService } from './users.service'
import { User } from './decorators/user.decorator'
import { UpdateUserDto } from './dto/update-user.dto'

@Controller('users')
export class UsersController {
	constructor(private readonly usersService: UsersService) {}

	@Get('profile')
	async getCurrentUser(@User('id') id: number) {
		return this.usersService.getUserById(id)
	}



	@Put('update')
	@HttpCode(200)
	async updateUser(@Body() id: number, dto: UpdateUserDto) {
		return this.usersService.updateUser(id, dto)
	}
}
