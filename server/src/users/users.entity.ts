import {
	Column,
	Entity,
	PrimaryGeneratedColumn,
	UpdateDateColumn
} from 'typeorm'

@Entity('Users')
export class UsersEntity {
	@PrimaryGeneratedColumn()
	id: number

	@Column({ unique: true })
	email: string

	@Column({ select: false })
	password: string

	@Column()
	role: string

	@Column({ default: false, name: 'is_active' })
	isActive: boolean

	@UpdateDateColumn({ name: 'updated_at' })
	updatedAt: Date
}
