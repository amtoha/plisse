import {
	BadRequestException,
	Injectable,
	NotFoundException
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { UsersEntity } from './users.entity'
import { Repository } from 'typeorm'
import { UpdateUserDto } from './dto/update-user.dto'
import { genSalt, hash } from 'bcryptjs'

@Injectable()
export class UsersService {
	constructor(
		@InjectRepository(UsersEntity)
		private readonly usersRepository: Repository<UsersEntity>
	) {}

	async getUserById(id: number) {
		const user = await this.usersRepository.findOneBy({ id })
		if (!user) throw new NotFoundException('Пользователь не найден')
		return user
	}

	async updateUser(id: number, dto: UpdateUserDto) {
		const user = await this.getUserById(id)

		if (dto.email) {
			const isSameUser = await this.usersRepository.findOneBy({
				email: dto.email
			})
			if (isSameUser && id !== isSameUser.id)
				throw new BadRequestException(
					'Пользователь с таким email уже есть в системе'
				)
			user.email = dto.email
		}

		if (dto.password) {
			const salt = await genSalt(10)
			user.password = await hash(dto.password, salt)
		}

		if (dto.role) user.role = dto.role
		if (dto.isActive) user.isActive = dto.isActive

		await this.usersRepository.save(user)
		return
	}
}
