import { createParamDecorator, ExecutionContext } from '@nestjs/common'
import { UsersEntity } from '../users.entity'

type TypeData = keyof UsersEntity

export const User = createParamDecorator(
	(data: TypeData, ctx: ExecutionContext) => {
		const request = ctx.switchToHttp().getRequest()
		const user = request.user
		return data ? user[data] : user
	}
)
