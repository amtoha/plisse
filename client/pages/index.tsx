import type { NextPage } from 'next'
import Head from 'next/head'

import Home from '@/screens/home/Home'

const HomePage: NextPage = () => {
	return (
		<>
			<Head>
				<title>Тут будет мега пушка</title>
			</Head>
			<Home />
		</>
	)
}

export default HomePage
