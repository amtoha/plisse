import { configureStore } from "@reduxjs/toolkit";
import { productsApi } from "./products/products.api";
import { cartReducer } from "./cart/sart.slice";

export const store = configureStore({
  reducer: {
    [productsApi.reducerPath]: productsApi.reducer,
    cart: cartReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(productsApi.middleware),
});

export type RootState = ReturnType<typeof store.getState>;
