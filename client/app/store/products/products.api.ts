import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { IProduct } from "./products.types";

export const productsApi = createApi({
  reducerPath: "api/products",
  baseQuery: fetchBaseQuery({ baseUrl: "https://fakestoreapi.com/" }),
  endpoints: (build) => ({
    getAllProducts: build.query<IProduct[], number>({
      query: (limit = 5) => `products?limit=${limit}`,
    }),
  }),
});

export const { useGetAllProductsQuery } = productsApi;
