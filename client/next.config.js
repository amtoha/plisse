/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	swcMinify: true,
	poweredByHeader: false,
	env: {
		MORALIS_SERVER_URL: process.env.MORALIS_SERVER_URL,
		MORALIS_APP_ID: process.env.MORALIS_APP_ID
	},
	async rewrites() {
		return [
			{ source: '/api/:path*', destination: 'http://localhost:4200/api/:path*' }
		]
	}
}

module.exports = nextConfig
